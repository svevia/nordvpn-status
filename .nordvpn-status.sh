#!/bin/bash
echo "<img>/usr/share/icons/NordVPN/16x16/nordvpn.png</img>"
#local status
status=$(nordvpn status | grep "Status" |  cut -f2 -d':'| sed 's/ //g')
color="<span weight='Bold' fgcolor='Red'> "
action="nordvpn c romania"
if [[ $status == "Connected" ]]; then
	color="<span weight='Bold' fgcolor='Green'>"
	action="nordvpn d"	
fi
echo "<txt>$color $status </span></txt>"
echo "<tool>$(nordvpn status)</tool>"
echo "<click>$action</click>"
