This script will give you a monitor that will show your norvpn connection status and let you enable of disable the connection in one click.

![](screenshots/screenshot.png)

To enable this monitor, right click on a XFCE panel > go to **Panel** > **Add New Items**
Select **Generic monitor**
Right click on the new monitor that appear then click on **properties*.

In command put the path to the .nordvpn-status.sh script and move the nordvpn.png to /usr/share/icons/NordVPN/16x16/nordvpn.png

By default the tool connect you to romania when you click on the logo, yu can change the destination on the connect command in the script.

